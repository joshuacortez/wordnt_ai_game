'''
Test the Wordnt Environment
'''
from game_env import WordntEnv

def main():
    env = WordntEnv(3)

    state = env.reset()
    print(state)

    state = env.play_action("add_to_start", "f")
    print(state)

    state = env.play_action("add_to_start", "f")
    print(state)

    state = env.play_action("add_to_end", "l")
    print(state)

    state = env.play_action("add_to_end", "i")
    print(state)

    # NOTE: test invalid action add 2 characters
    # state = env.play_action("add_to_end", "ii")
    # print(state)

    state = env.play_action("challenge_no_word")
    print(state)

    # NOTE: test invalid action, don't respond to challenge
    # state = env.play_action("add_to_end", "i")
    # print(state)

    # NOTE: test invalid action, not in ACTION_TYPES
    # state = env.play_action("efefe")
    # print(state)

    # NOTE: test a correct claim_word
    # state = env.play_action("claim_word", "shuffling")
    # print(state)

    # NOTE: test a wrong claim_word
    state = env.play_action("claim_word", "shufflilelele")
    print(state)

if __name__ == "__main__":
    main()