'''
Utility to create an animation of a running game
'''
import os
import numpy as np
import cv2
import textwrap
from utils import prettify_action, prettify_loss_condition

class WordntAnimator:

    def __init__(self, env, players, **kwargs):
        self.env = env
        self.players = players

        self.path = kwargs.get("path", "./videos/game.mp4")
        self.dim = kwargs.get("dim", [1000,500])
            # NOTE: x then y
        self.fps = kwargs.get("fps", 30)

        # NOTE: these are expressed as percentage of y dimension
        self.y_margin = kwargs.get("y_margin", 0.15)
        self.radius = kwargs.get("radius", 0.05)

        # NOTE: colors are in BGR
        self.player_color_default = kwargs.get("player_color_default", (255, 204, 204))
        self.player_color_turn = kwargs.get("player_color_turn", (0, 255, 0))
        self.string_color = kwargs.get("string_color", (255,191,128))
        self.loss_color = kwargs.get("loss_color", (102, 102, 255))

        self.reset()

    def set_players(self, players):
        self.players = players

    def reset(self):
        # TODO: make an opencv video object
        fourcc = cv2.VideoWriter_fourcc(*'XVID')

        self.vid = cv2.VideoWriter(
            self.path,
            fourcc = fourcc, 
            # fourcc = 0x7634706d,
            fps = self.fps,
            # frameSize = tuple(self.dim),
            frameSize = (self.dim[0], self.dim[1]),
            apiPreference = cv2.CAP_ANY,
        )

        # TODO: compute player locations
        n_cols = int(np.ceil(len(self.players)//2))
        
        center_interval = int(self.dim[0]/(n_cols + 1))
        y_margin_px = int(self.dim[1]*self.y_margin)
        self.radius_px = int(self.dim[1]*self.radius)
        
        self.player_center_locs = []

        # NOTE: centers are x from left, y from top
        for j in range(n_cols):
            self.player_center_locs.append(tuple([(j + 1)*center_interval, y_margin_px]))

        for j in range(n_cols):
            self.player_center_locs.append(tuple([self.dim[0] - (j + 1)*center_interval, self.dim[1] - y_margin_px]))

        self.wrapper = textwrap.TextWrapper(width = 30)
        self.subtitle_wrapper = textwrap.TextWrapper(width = 60)

        self.soft_reset()

    def soft_reset(self, loss_count = None):
        '''
        Reset between games in a single match.
        '''
        # NOTE: label each player with the last action they did
        self.player_last_actions = ["" for p in self.players]
        self.loss_count = loss_count

    def get_video(self):
        return self.vid

    def set_video(self, vid):
        self.vid = vid

    def _game_frame(self, game_state):
        # start with an empty black frame
        frame = np.zeros([self.dim[1], self.dim[0]] + [3], dtype = np.uint8)

        if game_state["last_turn"] is not None:
            self.player_last_actions[game_state["last_turn"]] = prettify_action(*game_state["last_action"])

        # put circles for the players
        for i, player in enumerate(self.players):
            cv2.circle(
                frame,
                center = self.player_center_locs[i],
                radius = self.radius_px,
                color = self.player_color_turn if i == game_state["current_turn"] else self.player_color_default,
                thickness = -1,
            )

            # TODO: add names on top of players
            cv2.putText(
                frame,
                player.name,
                org = (self.player_center_locs[i][0] - self.radius_px, self.player_center_locs[i][1] - int(self.radius_px*1.8)), # NOTE: coordinates of lower left
                fontFace = cv2.FONT_HERSHEY_SIMPLEX,
                fontScale = 0.8,
                color = self.player_color_default,
            )

            # TODO: add cumulative losses under player name
            if self.loss_count is not None:
                cv2.putText(
                    frame,
                    "Loss Count: {}".format(self.loss_count[i]),
                    org = (self.player_center_locs[i][0] - self.radius_px, self.player_center_locs[i][1] - int(self.radius_px*1.1)), # NOTE: coordinates of lower left
                    fontFace = cv2.FONT_HERSHEY_SIMPLEX,
                    fontScale = 0.4,
                    color = self.player_color_default,
                )

            # TODO: add last actions below players
            last_action_txt = self.wrapper.wrap("Last Action: " + self.player_last_actions[i])

            for j, line in enumerate(last_action_txt):
                cv2.putText(
                    frame,
                    line,
                    org = (self.player_center_locs[i][0] - self.radius_px, self.player_center_locs[i][1] + int(self.radius_px*1.4) + int(j*self.dim[0]*0.015)), # NOTE: coordinates of lower left
                    fontFace = cv2.FONT_HERSHEY_SIMPLEX,
                    fontScale = 0.4,
                    color = self.player_color_default,
                    bottomLeftOrigin = False,
                )

        # TODO: write string at the middle
        if game_state["loss_condition"] == "challenge_no_word_failed":
            # if a claimed word is valid, display it
            cv2.putText(
                frame,
                game_state["last_action"][1],
                org = (int(self.dim[0]*0.2), int(self.dim[1]*0.6)),
                fontFace = cv2.FONT_HERSHEY_SIMPLEX,
                fontScale = 3,
                thickness = 2,
                color = self.string_color,
            )
        else:
            cv2.putText(
                frame,
                game_state["current_string"],
                org = (int(self.dim[0]*0.2), int(self.dim[1]*0.6)),
                fontFace = cv2.FONT_HERSHEY_SIMPLEX,
                fontScale = 3,
                thickness = 2,
                color = self.string_color,
            )

        # TODO: add some indicator to show if someone lost and why
        if game_state["loser"] is not None:
            text_ = self.wrapper.wrap("Player {} lost due to {}".format(
                self.players[game_state["loser"]].name,
                prettify_loss_condition(game_state["loss_condition"]),
            ))
            for j, line in enumerate(text_):
                cv2.putText(
                    frame,
                    line,
                    org = (int(self.dim[0]*0.3), int(self.dim[1]*0.65) + int(j*self.dim[0]*0.02)), # NOTE: coordinates of lower left
                    fontFace = cv2.FONT_HERSHEY_SIMPLEX,
                    fontScale = 0.8,
                    color = self.loss_color,
                    bottomLeftOrigin = False,
                )

        return frame

    def _game_start_frame(self, subtitle = ""):
        '''
        Return a frame to show when a game starts
        '''
        frame = np.zeros([self.dim[1], self.dim[0]] + [3], dtype = np.uint8)
        
        # TODO: Write a title
        cv2.putText(
            frame,
            "WORDN'T! Game Starting...",
            org = (int(self.dim[0]*0.1), int(self.dim[1]*0.55)),
            fontFace = cv2.FONT_HERSHEY_SIMPLEX,
            fontScale = 2,
            thickness = 2,
            color = self.string_color,
        )

        # TODO: Write the subtitle below 
        text_ = self.subtitle_wrapper.wrap(subtitle)
        for j, line in enumerate(text_):
            cv2.putText(
                frame,
                line,
                org = (int(self.dim[0]*0.1), int(self.dim[1]*0.65) + int(j*self.dim[0]*0.02)), # NOTE: coordinates of lower left
                fontFace = cv2.FONT_HERSHEY_SIMPLEX,
                fontScale = 0.8,
                color = self.string_color,
                bottomLeftOrigin = False,
            )

        return frame

    def _loss_summary_frame(self, players, loss_count):
        self.set_players(players)

        # start with an empty black frame
        frame = np.zeros([self.dim[1], self.dim[0]] + [3], dtype = np.uint8)

        cv2.putText(
            frame,
            "Final Result:",
            org = (int(self.dim[0]*0.2), int(self.dim[1]*0.2)), # NOTE: coordinates of lower left
            fontFace = cv2.FONT_HERSHEY_SIMPLEX,
            fontScale = 0.8,
            thickness = 2,
            color = self.player_color_default,
        )

        for i, player in enumerate(self.players):
            cv2.putText(
                frame,
                "Player {} lost {} times".format(player.name, loss_count[i]),
                org = (int(self.dim[0]*0.2), int(self.dim[1]*0.25 + i*self.dim[1]*0.05)), # NOTE: coordinates of lower left
                fontFace = cv2.FONT_HERSHEY_SIMPLEX,
                fontScale = 0.8,
                color = self.player_color_default,
            )
        return frame

    def add_frame(self, game_state, n_seconds = 3):
        '''
        Just add the same frame over and over again
        '''
        frame = self._game_frame(game_state)

        n_frames = int(n_seconds*self.fps)

        # TODO: add to output video
        for i in range(n_frames):
            self.vid.write(frame)

    def display_frame(self, game_state):
        frame = self._game_frame(game_state)

        cv2.imshow('Wordnt', frame)
        cv2.waitKey(0)

    def add_start_frame(self, subtitle = "", n_seconds = 3):
        frame = self._game_start_frame(subtitle = subtitle)
        n_frames = int(n_seconds*self.fps)

        # TODO: add to output video
        for i in range(n_frames):
            self.vid.write(frame)

    def display_start_frame(self, subtitle = ""):
        frame = self._game_start_frame(subtitle = subtitle)

        cv2.imshow('Wordnt', frame)
        cv2.waitKey(0)

    def add_loss_summary_frame(self, players, loss_count, n_seconds = 3):
        frame = self._loss_summary_frame(players, loss_count)
        n_frames = int(n_seconds*self.fps)

        # TODO: add to output video
        for i in range(n_frames):
            self.vid.write(frame)

    def display_loss_summary_frame(self, players, loss_count):
        frame = self._loss_summary_frame(players, loss_count)

        cv2.imshow('Wordnt', frame)
        cv2.waitKey(0)

    def close(self):
        '''
        Close the video file
        '''
        self.vid.release()