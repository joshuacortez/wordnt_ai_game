'''
Test the sample agent playing against itself
'''
import os
import time
import random

from utils import TimedOutExc, ActionExc, timeout, pretty_print_state
from game_env import WordntEnv
from agents import SampleAgent
from agents import AgentA
from agents import AgentB
from agents import AgentC
# from agents import AgentD
from agents import AgentE
from agents import AgentF
from animation import WordntAnimator

N_GAMES = 3
MAX_CONSTRUCT_TIME = 180
MAX_ACTION_TIME = 30
SLEEP_TIME = 0
DISPLAY = True

AGENT_CLASSES = [
    AgentA,
    AgentB,
    AgentC,
    SampleAgent,
    AgentE,
    AgentF,
]
AGENT_NAMES = [
    "Alpha",
    "Bravo",
    "Charlie",
    "Delta",
    "Echo",
    "Foxtrot",
]

@timeout(MAX_CONSTRUCT_TIME)
def construct_agent(agent_class, *args, **kwargs):
    return agent_class(*args, **kwargs)

@timeout(MAX_ACTION_TIME)
def get_agent_action(agent_, state_): 
    return agent_.get_action(state_)

def main():
    
    # TODO: show animation while playing,
    # automatically move
    # while also recording video

    players = []
    for i, agent_class in enumerate(AGENT_CLASSES):
        try:
            next_agent = construct_agent(
                agent_class, 
                name = AGENT_NAMES[i],
                n_players = len(AGENT_NAMES),
            )
            players.append(next_agent) 
        except TimedOutExc:
            print("Player {} was disqualified due to exceeding the time limit for agent construction.".format(AGENT_NAMES[i]))
        except:
            print("Player {} was disqualified due to an error during agent construction.".format(AGENT_NAMES[i]))

    if len(players) < 1:
        return

    losses = [0 for i in range(len(players))]

    env = WordntEnv(len(players))
    animator = WordntAnimator(
        env,
        players,
        path = "./videos/test_multiple_games.avi"
    )

    for game_idx in range(N_GAMES):
        print("====================")
        print("Starting game {}.".format(game_idx + 1))

        loser = play_game(
            env, 
            players, 
            animator = animator, 
            game_num = game_idx,
            loss_count = losses,
            display = DISPLAY,
        )
        if loser == "All":
            for i in range(len(AGENT_NAMES)):
                losses[i] += 1
        else:
            losses[loser] += 1

    print("====================")
    for i in range(len(players)):
        print("Player {} lost {} times.".format(players[i].name, losses[i]))

    animator.add_loss_summary_frame(players, losses)
    # NOTE: display
    if DISPLAY:
        animator.display_loss_summary_frame(players, losses)

    animator.close()

def play_game(env, players, verbosity = 1, animator = None, game_num = 0, loss_count = None, display = False):

    state = env.reset()
    loser = None
    play_order = [i for i in range(len(players))]
    random.shuffle(play_order)
    ordered_players = [players[i] for i in play_order]
    
    if animator is not None:
        ordered_loss_count = [loss_count[i] for i in play_order]
        animator.soft_reset(loss_count = ordered_loss_count)
        animator.set_players(ordered_players)
    
    if verbosity > 0:
        pretty_print_state(state, ordered_players)

    if verbosity > 0:
        sequence_str = "Turn sequence is "
        for i, player_ in enumerate(ordered_players):
            if i > 0:
                sequence_str += "then "
            sequence_str += player_.name
            if i == len(ordered_players) - 1:
                sequence_str += "."
            else:
                sequence_str += " "
        print(sequence_str)

    if animator is not None:
        subtitle = "Game number {}. Player Sequence: ".format(game_num + 1)
        subtitle += ", ".join([ag.name for ag in ordered_players])

        animator.add_start_frame(subtitle = subtitle)
        if display:
            animator.display_start_frame(subtitle = subtitle)
        animator.add_frame(state)
        if display:
            animator.display_frame(state)

    while not state["done"]:
        current_player_idx = state["current_turn"]
        current_agent = ordered_players[current_player_idx]

        try:
            # NOTE: using a wrapper function which has a timeout exception
            action_type, string_ = get_agent_action(current_agent, state)
        except ActionExc:
            if verbosity > 0:
                print("Player {} lost the game due to an error in the agent.".format(current_agent.name))
            # NOTE: return the index in the original order
            loser = play_order[current_player_idx]
            break
        except TimedOutExc:
            if verbosity > 0:
                print("Player {} lost the game due to exceeding the time limit.".format(current_agent.name))
            # NOTE: return the index in the original order
            loser = play_order[current_player_idx]
            break

        new_state = env.play_action(action_type, string_)

        if animator is not None:
            animator.add_frame(new_state)
            if display:
                animator.display_frame(new_state)

        if verbosity > 0:
            pretty_print_state(new_state, ordered_players)

        state = new_state
        time.sleep(SLEEP_TIME)

    if loser is None:
        # NOTE: return the index in the original order
        loser = play_order[state["loser"]]

    return loser

def test_animation():

    players = []
    for i, agent_class in enumerate(AGENT_CLASSES):
        next_agent = construct_agent(agent_class, name = AGENT_NAMES[i])
        players.append(next_agent) 

    env = WordntEnv(len(players))

    animator = WordntAnimator(
        env,
        players,
        path = "./videos/test_game.avi"
    )

    state = env.reset()

    subtitle = "Game number 1. Player Sequence: "
    subtitle += ", ".join([ag.name for ag in players])

    animator.display_start_frame(subtitle = subtitle)
    animator.add_start_frame(subtitle = subtitle)

    animator.display_frame(state)
    animator.add_frame(state)
    while not state["done"]:
        current_player_idx = state["current_turn"]
        current_agent = players[current_player_idx]

        # NOTE: using a wrapper function which has a timeout exception
        action_type, string_ = get_agent_action(current_agent, state)
        new_state = env.play_action(action_type, string_)

        animator.display_frame(new_state)
        animator.add_frame(new_state)

        state = new_state

        #TEMP
        # break

    animator.close()

if __name__ == "__main__":
    main()
    # test_animation()